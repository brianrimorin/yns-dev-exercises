<html>
  <body>
	<?php
      //Using https://www.php.net/manual/en/tutorial.forms.php as a base
      $firstName = htmlspecialchars($_POST['firstName']);
      $lastName = htmlspecialchars($_POST['lastName']);
      $birthday = new DateTime($_POST['birthday']);
      $email = htmlspecialchars($_POST['email']);

	    echo "Name: $firstName $lastName <br>";
      echo 'Date of Birth: ' . $birthday->format('Y-m-d') . '<br>';
      echo "Email Address: $email <br>";
	?>
  </body>
</html>