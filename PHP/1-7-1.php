<?php
    session_start();
?>
<html>
  <body>
	<?php
        $firstName = $_SESSION['firstName'];
        $lastName = $_SESSION['lastName'];
        $birthday = new DateTime($_SESSION['birthday']);
        $email = $_SESSION['email'];
        $phone = $_SESSION['phone'];

        echo "Name: $firstName $lastName <br>";
        echo 'Date of Birth: ' . $birthday->format('Y-m-d') . '<br>';
        echo "Email Address: $email <br>";
        echo "Phone Number: $phone <br>";
	?>
  </body>
</html>