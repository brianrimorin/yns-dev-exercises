<html>
  <style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    }
  </style>
  <body>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Date of Birth</th>
        <th>Email Address</th>
        <th>Phone Number</th>
      </tr>
	<?php
        $file = fopen('data.csv', 'r');
        while (($data = fgetcsv($file, 1000, ",")) !== false) {
            echo '<tr>';
            foreach($data as $field) {
                echo "<td> $field </td>";
            }
            echo '</tr> <br>';
        }
	?>
    </table>
  </body>
</html>