<html>
  <body>
    <form method = "post">
	  Number: <input type = "number" name = "number"> <br>
	  <input type = "submit" name = "submit" value = "Submit">
	</form>
	<?php
	    if(isset($_POST['submit'])) {
		    $number = $_POST['number'];		
			for ($i = 1; $i <= $number; $i++){
                if ($i % 3 == 0 && $i % 5 == 0) {
                    echo 'FizzBuzz <br>';
                    continue;
                }

                if ($i % 3 == 0) {
                    echo 'Fizz <br>';
                    continue;
                }

                if ($i % 5 == 0) {
                    echo 'Buzz <br>';
                    continue;
                }

                echo "$i <br>";
            }
		}
	?>
  </body>
</html>