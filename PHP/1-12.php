<html>
  <style>
    table, th, td {
    border: 1px solid black;
    }
  </style>
  <body>
  <?php
        if (isset($_SESSION)) {
          $firstName = $_SESSION['firstName'];
          $lastName = $_SESSION['lastName'];
        }
        ?>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Date of Birth</th>
        <th>Email Address</th>
        <th>Phone Number</th>
        <th>Profile Image</th>
      </tr>
      <?php
          $file = fopen('resources/data.csv', 'r');

          $pointer; //refers to the first row that will be displayed (range: 1 to maxRows-10)
          $maxRows = countUsers(); //number of total rows in data.csv
          $row = 1; //used to count which row is currently being processed
          define('PAGE_SIZE', 10); //number of entries per page

          //Getting stored value of $pointer as passed by buttons via url variables
          //Currently works if a value for pointer is provided in browser, but not from links
          if (!empty($_GET['pointer'])) {
            $pointer = (int) $_GET['pointer'];
          } else {
            $pointer = 1;
          }

          //adjusting $pointer to make sure it doesn't go out of bounds
          if ($pointer < 1) {
            $pointer = 1;
          }

          if ($pointer + PAGE_SIZE > $maxRows) {
            $pointer = $maxRows - PAGE_SIZE + 1;
          }

          $displayedRows = 0;
          while (($data = fgetcsv($file, 0, ",")) !== false) {
              if ($displayedRows == PAGE_SIZE) {
                break;
              }

              if ($row >= $pointer) {
                echo '<tr>';
                $column = 1;
                foreach($data as $field) {
                    if ($column % 6 == 0){
                      $filename = 'resources/' . $field;
                        echo "<td> <img src='$filename' height='50'> </td>";
                        $column = 1;
                        break;
                    }

                    echo "<td> $field </td>"; 
                    $column++;
                }
                echo '</tr>';
                $displayedRows++;
              }

              $row++;
          }

          echo '</table>';

          function countUsers() { 
            //Counts number of rows in csv, in order to append an appropriate user number for new user
            $rows = 0;
            $file = fopen('resources/data.csv', 'r');
            while (fgetcsv($file)) {
                $rows++;
            }
            
            fclose($file);
            return $rows;
          }

          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($pointer - PAGE_SIZE) . '>Previous</a> ';
          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($pointer + PAGE_SIZE) . '>Next</a>';
    ?>


  </body>
</html>