<?php
    session_start();
?>
<html>
  <body>
    <div>
      <img src="<?php echo $_SESSION['avatar']; ?>" alt="picture"/>
    </div>
	<?php
        $firstName = $_SESSION['firstName'];
        $lastName = $_SESSION['lastName'];
        $birthday = $_SESSION['birthday'];
        $email = $_SESSION['email'];
        $phone = $_SESSION['phone'];

        $userData = array($firstName, $lastName, $birthday, $email, $phone);
        $file = fopen('data.csv', 'a');
        fputcsv($file, $userData);
        fclose($file);
        echo 'Data saved!';
	?>
  </body>
</html>