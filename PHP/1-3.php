<html>
  <body>
    <form method = "post">
	  First Number: <input type = "number" name = "firstNumber"> <br>
	  Second Number: <input type = "number" name = "secondNumber"> <br>
	  <input type = "submit" name = "submit" value = "Compute GCD">
	</form>
	<?php
	    if(isset($_POST['submit'])) {
		    $a = $_POST['firstNumber'];		
		    $b = $_POST['secondNumber'];
			/**
             *Using Eclidean Algorithm
             *Both a and b cannot be zero
            */

            if ($a == 0 && $b == 0) {
                echo 'Result: Invalid Operands';
                return;
            }
            
            while ($b != 0) {
                $temporaryB = $b;
                $b =  $a % $b;
                $a = $temporaryB;
            }
            
            echo "Result: <input type='text' value = $a>";
		}
	?>
  </body>
</html>