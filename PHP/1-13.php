<?php
    session_start();
    //clear session variables (in case of redirect from logout)
    $_SESSION['firstName'] = '';
    $_SESSION['avatar'] = '';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (authenticate($_POST['username'], $_POST['password']) == true) {
            header('Location:1-13-2.php');
        }
    }

    function authenticate($username, $password) {
        $file = fopen('resources/data_new.csv', 'r');
        $tempUsername = $tempPassword = '';
        while (($data = fgetcsv($file, 0, ",")) !== false) {
            foreach($data as $field) {
                $tempUsername = $data[0];
                $tempPassword = $data[1];                
                if ($tempUsername != $username) {
                    break;
                }   

                if ($tempPassword != $password) {
                    break;
                }
                
                //copy first name and avatar
                $_SESSION['firstName'] = $data[3];
                $_SESSION['avatar'] = 'resources/' . $data[7];
                return true;
            }

        }
        echo 'Incorrect username/password!';
        return false;
    }
?>
<html>
  <body>
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = 'post'>
        <label for="user">Username:</label><br>
        <input type="text" id="username" name="username"><br>
        <label for="password">Password:</label><br>
        <input type="password" id="password" name="password"><br><br>
        <input type="submit" value="Submit">
    </form>
    <br> <a href='1-13-1.php'>Register</a>
  </body>
</html>