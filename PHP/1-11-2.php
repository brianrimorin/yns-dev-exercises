<html>
  <style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    }
  </style>
  <body>
    <table>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Date of Birth</th>
        <th>Email Address</th>
        <th>Phone Number</th>
        <th>Profile Image</th>
      </tr>
      <?php
            $file = fopen('resources/data.csv', 'r');
            $column = 1;
            while (($data = fgetcsv($file, 0, ",")) !== false) {
                echo '<tr>';
                foreach($data as $field) {
                    if ($column % 6 == 0){
                      $filename = 'resources/' . $field;
                        echo "<td> <img src='$filename' height='100'> </td>";
                        $column = 1;
                        break;
                    }

                    echo "<td> $field </td>";
                    $column++;
                }
                echo '</tr> <br>';
            }
      ?>
    </table>
  </body>
</html>