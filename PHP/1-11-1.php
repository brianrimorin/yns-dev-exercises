<?php
    session_start();
?>
<html>
  <body>
	<?php
        $firstName = $_SESSION['firstName'];
        $lastName = $_SESSION['lastName'];
        $birthday = $_SESSION['birthday'];
        $email = $_SESSION['email'];
        $phone = $_SESSION['phone'];
        $avatar = substr($_SESSION['avatar'], 10); //removing 'resources/'

        $userData = array($firstName, $lastName, $birthday, $email, $phone, $avatar);
        $file = fopen('resources/data.csv', 'a');
        fputcsv($file, $userData);
        fclose($file);
        echo 'Data saved!';
	?>
    <a href="1-11-2.php">See user info</a>
  </body>
</html>