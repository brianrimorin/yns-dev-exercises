<?php
    session_start();
?>
<html>
  <body>
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = 'post'>
        First Name: <input type = 'text' name = 'firstName'> <br>
        Last Name: <input type = 'text' name = 'lastName'> <br>
        Date of Birth: <input type = 'date' name = 'birthday'> <br>
        Email Address: <input type = 'text' name = 'email'> <br>
        Phone Number: <input type = 'number' name = 'phone'> <br>
        <input type = 'submit'>
    </form>
	<?php
	    /* Validation requirements:
       * First Name, Last Name: text and whitespace only
       * Date of Birth: Non-empty date
       * Email Address: @ symbol and a . after it
       * Phone Number: Number only (might already be fulfilled by "type = 'number'"), 8 digits
       */

      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          if (validate() == true) {
              //Then send this data back to 613-output.php
              //Using session ahead of schedule (before 1-13) in order to store validated input data to pass to 613-output.php
              $_SESSION['firstName'] = $_POST['firstName'];
              $_SESSION['lastName'] = $_POST['lastName'];
              $_SESSION['birthday'] = $_POST['birthday'];
              $_SESSION['email'] = $_POST['email'];
              $_SESSION['phone'] = $_POST['phone'];

              header('Location:1-8-1.php');
          } 
      }

      function validate() {
          //Run validation checks
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['firstName'])) {
              echo "Invalid First Name";
              return false;
          }
          
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['lastName'])) {
            echo "Invalid Last Name";
            return false;
          }

          if (empty($_POST["birthday"]) == true) {
            echo "Invalid Date of Birth";
            return false;
          }
          
          //Naive email validation (alphanumerics and -+_@.)
          if (!preg_match ('/^[a-zA-Z0-9\-+@.]+$/', $_POST['email'])) {
            echo "Invalid Email Address";
            return false;
          }
          
          define('PHONE_NUMBER_LENGTH', 8);
          if (!strlen($_POST['phone']) == PHONE_NUMBER_LENGTH) {
            echo "Invalid Phone Number";
            return false;
          }

          return true;
      }
	?>
  </body>
</html>