<?php
    session_start();
?>
<html>
  <body>
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = 'post' enctype = 'multipart/form-data'>
        First Name: <input type = 'text' name = 'firstName'> <br>
        Last Name: <input type = 'text' name = 'lastName'> <br>
        Date of Birth: <input type = 'date' name = 'birthday'> <br>
        Email Address: <input type = 'text' name = 'email'> <br>
        Phone Number: <input type = 'number' name = 'phone'> <br>
        Profile Image: <input type = 'file' name = 'avatar'> <br>
        <input type = 'submit'>
    </form>
	<?php
      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          if (validate() == true) {
              $_SESSION['firstName'] = $_POST['firstName'];
              $_SESSION['lastName'] = $_POST['lastName'];
              $_SESSION['birthday'] = $_POST['birthday'];
              $_SESSION['email'] = $_POST['email'];
              $_SESSION['phone'] = $_POST['phone'];


              $filename    = $_FILES['avatar']['tmp_name'];
              $destination = 'resources/' . $_FILES['avatar']['name']; 
              move_uploaded_file($filename, $destination);
              $_SESSION['avatar'] = $destination;

              header('Location:1-10-1.php');
          } 
      }

      function validate() {
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['firstName'])) {
              echo 'Invalid First Name';
              return false;
          }
          
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['lastName'])) {
            echo 'Invalid Last Name';
            return false;
          }

          if (empty($_POST["birthday"]) == true) {
            echo 'Invalid Date of Birth';
            return false;
          }
          
          if (!preg_match ('/^[a-zA-Z0-9\-+@.]+$/', $_POST['email'])) {
            echo 'Invalid Email Address';
            return false;
          }
          
          define('PHONE_NUMBER_LENGTH', 8);
          if (!strlen($_POST['phone']) == PHONE_NUMBER_LENGTH) {
            echo 'Invalid Phone Number';
            return false;
          }
          
          $tempFile = getimagesize(($_FILES['avatar']['tmp_name']));
          
          if ($tempFile === false){
            echo 'Invalid file selected';
            return false;
          }

          return true;
      }
	?>
  </body>
</html>