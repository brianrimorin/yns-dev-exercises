SELECT * FROM test
ORDER BY CASE 
     WHEN id = 1 THEN 1
     WHEN id = 4 THEN 2
	 WHEN id = 3 THEN 3
	 WHEN id = 6 THEN 4
	 WHEN id = 7 THEN 5
	 WHEN id = 2 THEN 6
	 WHEN id = 5 THEN 7
     ELSE 8 
     END ASC