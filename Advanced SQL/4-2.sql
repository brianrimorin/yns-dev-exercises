-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2022 at 06:09 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `4-2`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_shifts`
--

CREATE TABLE `daily_work_shifts` (
  `id` int(1) DEFAULT NULL,
  `therapist_id` int(1) DEFAULT NULL,
  `target_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_work_shifts`
--

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES
(1, 1, '2022-03-17', '14:00:00', '15:00:00'),
(2, 2, '2022-03-17', '22:00:00', '23:00:00'),
(3, 3, '2022-03-17', '00:00:00', '01:00:00'),
(4, 4, '2022-03-17', '05:00:00', '05:30:00'),
(5, 1, '2022-03-17', '21:00:00', '21:45:00'),
(6, 5, '2022-03-17', '05:00:00', '05:20:00'),
(7, 3, '2022-03-17', '14:00:00', '15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `therapists`
--

CREATE TABLE `therapists` (
  `id` int(1) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `therapists`
--

INSERT INTO `therapists` (`id`, `name`) VALUES
(1, 'John'),
(2, 'Arnold'),
(3, 'Robert'),
(4, 'Ervin'),
(5, 'Smith');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
