<?php
    session_start();
?>
<html>
  <body>
    <?php 
        if (!$_POST) {
            $_SESSION['questionNumber'] = 0;
            $_SESSION['questionOrder'] = array();
            $_SESSION['answers'] = array();
            $_SESSION['quizLength'] = 10;

            //Populate questionOrder

            for ($i = 1; $i <=  $_SESSION['quizLength']; $i++) {
                array_push($_SESSION['questionOrder'], $i);
            }

            shuffle($_SESSION['questionOrder']);

            echo 'Question ' . ($_SESSION['questionNumber'] + 1) . '. ';
            displayQuestion($_SESSION['questionOrder'][$_SESSION['questionNumber']]);
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          $lastAnswer = $_POST['choice'];
          array_push($_SESSION['answers'], $lastAnswer);
          $_SESSION['questionNumber']++;

          if ($_SESSION['questionNumber'] ==  $_SESSION['quizLength']){
            header('Location:6-1-1.php');
          }
          echo 'Question ' . ($_SESSION['questionNumber'] + 1) . '. ';
          displayQuestion($_SESSION['questionOrder'][$_SESSION['questionNumber']]);
        }

        function displayQuestion($questionNumber) {
            //SQL Details
            $sqlServer = 'localhost'; //change to 'mysql-server' later
            $sqlUser = 'root';
            $sqlPassword = ''; //change to 'secret' later
            $sqlDatabase = '6-1';

            $conn = new mysqli($sqlServer, $sqlUser, $sqlPassword, $sqlDatabase);
            $_SESSION['conn'] = $conn;
            
            $result = $conn->query("SELECT question
                                    FROM questions
                                    WHERE id=$questionNumber;");

            while ($row = $result->fetch_assoc()) {
                echo $row['question'] . '<br>';
            }

            $result = $conn->query("SELECT choice
                                    FROM choices
                                    WHERE question_id=$questionNumber
                                    ORDER BY choice_order ASC;");
            $counter = 1;

            echo "<form method='post' action='" . htmlspecialchars($_SERVER['PHP_SELF']) . "'>";

            while ($row = $result->fetch_assoc()) {
                echo '<input type="radio" id="choice';
                echo $counter . '" name="choice" ';
                echo "value='$counter'>";
                echo '<label>' . $row['choice'] . '</label><br>';
                $counter++;
            }

            echo '<input type="submit" value="Submit"> </form>';
            $conn->close();
        }
    ?>  
  </body>
</html>