<!DOCTYPE html>
<html>
  
<body>

    <table>
        <tr>
            <th>Sun</th>
            <th>Mon</th>
            <th>Tue</th>
            <th>Wed</th>
            <th>Thu</th>
            <th>Fri</th>
            <th>Sat</th>
        </tr>
        <?php
            $offset = 0;
            if (!empty($_GET['offset'])) {
                $offset = (int) $_GET['offset'];
              } else {
                $offset = 0;
              } 
            
            $month = adjustMonthYear($offset)->format('m');
            $year = adjustMonthYear($offset)->format('Y');
            echo $year . '-' . $month . '<br>';
            calendar($month, $year);
            echo '<a href=' . $_SERVER['PHP_SELF'] . '?offset=' . ($offset - 1) . '>Previous Month</a>';
        ?>
    </table>
    <?php
        echo '<a href=' . $_SERVER['PHP_SELF'] . '?offset=' . ($offset + 1) . '>Next Month</a>';
    ?>
</body>
<?php
    function calendar($month, $year) {
        $maximum = cal_days_in_month(CAL_GREGORIAN, $month, $year); //gets the last day of the month (28, 29, 30, 31)
        $startDay = date('w', strtotime($year . '-' . $month . '-01')); //Gets the day of the week for the first day of the month (as int, with sunday = 0)
        $dayPointer = 0; //Current day to fill out?
        $currentMonth = date('m');
        $currentYear = date('Y');
        $currentDay = date('d');
        $thisMonth = ($currentMonth == $month && $currentYear == $year); //Checks if the month to display is the current month (and year)

        echo '<tr>';
        //Increment $dayPointer to offset to match actual day of the week for day 1
        for ($dayPointer; $dayPointer < $startDay; $dayPointer++){
            echo '<td></td>';
        }
        
        for ($i = 1; $i <= $maximum; $i++) {
            if ($dayPointer == 7) {
                $dayPointer = 0;
            }

            if ($dayPointer == 0) {
                echo '<tr>';
            }
            
            echo '<td>'; 
            //Highlight the current day
            if ($thisMonth && $i == $currentDay){
                echo "<mark>$i</mark>";
            } else {
                echo $i;
            } 

            echo '</td>';

            if ($dayPointer == 6) {
                echo '</tr>';
            }
            
            $dayPointer++;
        }
    }
    //uses the pointer passed from page traversals to properly increment the month (and year) to display 
    function adjustMonthYear($offset) {
        $date = new DateTime(date('Y-m'));
        if ($offset > 0){
            $date->add(new DateInterval('P'. $offset . 'M'));
        }
        
        if ($offset < 0) {
            $date->sub(new DateInterval('P'. -$offset . 'M'));
        }

        return $date;
    }
?>
</html>