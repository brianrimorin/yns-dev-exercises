<?php
    session_start();
?>
<html>
  <body>
    <?php
        $score = checkAnswers($_SESSION['answers'], $_SESSION['questionOrder']);

        echo "You answered " . $score . " out of " . $_SESSION['quizLength'] . " questions correctly.";
        session_destroy();

        function checkAnswers($answers, $questionOrder) {
            
            //SQL Details
            $sqlServer = 'localhost'; //change to 'mysql-server' later
            $sqlUser = 'root';
            $sqlPassword = ''; //change to 'secret' later
            $sqlDatabase = '6-1';

            $conn = new mysqli($sqlServer, $sqlUser, $sqlPassword, $sqlDatabase);
            
            $tempScore = 0;
            for ($i = 0; $i < $_SESSION['quizLength']; $i++) {
                $questionNumber = $_SESSION['questionOrder'][$i];
                $answer = $_SESSION['answers'][$i];
                
                $result = $conn->query("SELECT *
                                        FROM answers a
                                        LEFT JOIN choices c ON a.answer_id = c.id
                                        WHERE a.question_id = $questionNumber AND c.choice_order = $answer");

                if ($result->num_rows == 1) {
                        $tempScore++;
                }
            }
            
            $conn->close();
            return $tempScore;
        }
    ?>
  </body>
</html>