<?php
    session_start();
?>
<html>
  <style>
    table, th, td {
    border: 1px solid black;
    }
  </style>
  <body>
    <?php
        if (!empty($_SESSION['firstName'])) {
            echo '<p> Hello ' . $_SESSION['firstName'] . '</p>';
            echo '<img src="' . $_SESSION['avatar'] . '"height="100"/> <br>';
        }
    ?>
    <table>
      <tr>
        <th>Username</th>
        <th>Profile Image</th>
      </tr>
      <?php
          $pointer;
          define('PAGE_SIZE', 10);

          //SQL Details
          $sqlServer = 'mysql-server';
          $sqlUser = 'root';
          $sqlPassword = 'secret';
          $sqlDatabase = '3-5';

          $conn = new mysqli($sqlServer, $sqlUser, $sqlPassword, $sqlDatabase);

          //Naive counting of rows over the entire table
          $result = $conn->query("SELECT *
                                  FROM userdata;"
          );
          $maxRows = $result->num_rows;


          if (!empty($_GET['pointer'])) {
            $pointer = (int) $_GET['pointer'];
          } else {
            $pointer = 1;
          }

          //adjusting $pointer to make sure it doesn't go out of bounds
          if ($pointer < 1) {
            $pointer = 1;
          }

          if ($pointer + PAGE_SIZE > $maxRows) {
            $pointer = $maxRows - PAGE_SIZE + 1;
          }

          $row = 1; 
          $displayedRows = 0;
          $result = $conn->query("SELECT username, avatar 
                              FROM userdata;" 
          );

          while($data = $result->fetch_assoc()) {
            if ($displayedRows == PAGE_SIZE) {
              break;
            }

            if ($row >= $pointer) {
              echo '<tr>';
              $field = $data['username'];
              echo "<td> $field </td>";
              $filename = 'resources/' . $data['avatar'];
              echo "<td> <img src='$filename' height='50'> </td>";
              echo '</tr>';
              $displayedRows++;
            }

            $row++;

          }
          echo '</table>';
          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($pointer - PAGE_SIZE) . '>Previous</a> ';
          echo '<a href=' . $_SERVER['PHP_SELF'] . '?pointer=' . ($pointer + PAGE_SIZE) . '>Next</a>';
    ?>
    <br><a href=3-5.php>Logout</a>
  </body>
</html>
