<html>
    <head>
    </head>
    <body>
    <?php
        //PHP
        echo '<table>';
        echo '<tr>';
        echo '<th>PHP</th>';
        echo '</tr>';
        $file = fopen('resources/php.csv', 'r');
        while (($data = fgetcsv($file, 0, ",")) !== false) {
            echo '<tr>';
            echo "<td> <a href='/yns-dev-exercises/PHP/$data[0].php'> $data[0] </a> </td>";
            echo "<td> $data[1] </td>";
            echo '</tr>';
        }
        echo '</table>';
        echo '<br>';

        //JavaScript
        echo '<table>';
        echo '<tr>';
        echo '<th>JavaScript</th>';
        echo '</tr>';
        $file = fopen('resources/javascript.csv', 'r');
        while (($data = fgetcsv($file, 0, ",")) !== false) {
            echo '<tr>';
            echo "<td> <a href='/yns-dev-exercises/JavaScript/$data[0].html'> $data[0] </a> </td>";
            echo "<td> $data[1] </td>";
            echo '</tr>';
        }
        echo '</table>';
        echo '<br>';

        //Database
        echo '<table>';
        echo '<tr>';
        echo '<th>Database</th>';
        echo '</tr>';
        $file = fopen('resources/database.csv', 'r');
        $flag = true;
        while (($data = fgetcsv($file, 0, ",")) !== false) {
            echo '<tr>';
            if ($flag) {
                echo "<td> <a href='/yns-dev-exercises/SQL/$data[0].txt'> $data[0] </a> </td>";
                $flag = !$flag;
            } else {
                echo "<td> <a href='/yns-dev-exercises/SQL/$data[0].php'> $data[0] </a> </td>";
            }
            echo "<td> $data[1] </td>";
            echo '</tr>';
        }
        echo '</table>';
        echo '<br>';

        //Advanced SQL
        echo '<table>';
        echo '<tr>';
        echo '<th>Advanced SQL</th>';
        echo '</tr>';
        $file = fopen('resources/advanced.csv', 'r');
        while (($data = fgetcsv($file, 0, ",")) !== false) {
            echo '<tr>';
            echo "<td> <a href='/yns-dev-exercises/Advanced SQL/$data[0].txt'> $data[0] </a> </td>";
            echo "<td> $data[1] </td>";
            echo '</tr>';
        }
        echo '</table>';
        echo '<br>';

        //Docker
        ?>
        <table>
            <tr>
                <th>Docker</th>
            </tr>
            <tr>
                <td>5-2</td>
                <td>Create <a href='/yns-dev-exercises/Docker/php.Dockerfile'>Dockerfile </a>
                    and 
                    <a href='/yns-dev-exercises/Docker/docker-compose.yaml'>docker-compose.yml </a> <br>
                    Requirements: <br>
                    <ul><li>Containers:</li>
                        <ul><li>web-server</li>
                                <ul><li>PHP v7.4.3 with apache</li></ul>
                            <li>mysql-server</li>
                                <ul><li>mysql v8.0.19</li></ul>
                            <li>phpmyadmin</li>
                                <ul><li>phpmyadmin v5.0.1</li></ul>
                        </ul>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><a href='/yns-dev-exercises/Docker/3-5.php'> Modified 3-5</a></td>
                <td>Exercise must be accessible via docker.</td>
            </tr>

        </table>
        <?php
            //Practice Systems
            echo '<table>';
            echo '<tr>';
            echo '<th>Practice Systems/Programs</th>';
            echo '</tr>';
            $file = fopen('resources/practice.csv', 'r');
            while (($data = fgetcsv($file, 0, ",")) !== false) {
                echo '<tr>';
                echo "<td> <a href='/yns-dev-exercises/Practice_System/$data[0].php'> $data[0] </a> </td>";
                echo "<td> $data[1] </td>";
                echo '</tr>';
            }
            echo '</table>';
            echo '<br>';
        ?>
      
    </body>
</html>