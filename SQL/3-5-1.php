<?php
    session_start();
?>
<html>
  <body>
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = 'post' enctype = 'multipart/form-data'>
        Username: <input type = 'text' name = 'username'> <br>
        Password: <input type = 'password' name = 'password'> <br>
        First Name: <input type = 'text' name = 'firstName'> <br>
        Last Name: <input type = 'text' name = 'lastName'> <br>
        Date of Birth: <input type = 'date' name = 'birthday'> <br>
        Email Address: <input type = 'text' name = 'email'> <br>
        Phone Number: <input type = 'number' name = 'phone'> <br>
        Profile Image: <input type = 'file' name = 'avatar'> <br>
        <input type = 'submit'>
    </form>
	<?php
      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
          if (validate() == true) {
              $filename = $_FILES['avatar']['tmp_name'];
              $destination = 'resources/' . $_FILES['avatar']['name']; 
              move_uploaded_file($filename, $destination);

              $username = $_POST['username'];
              $password = $_POST['password'];
              $firstName = $_POST['firstName'];
              $lastName = $_POST['lastName'];
              $birthday = $_POST['birthday'];
              $email = $_POST['email'];
              $phone = $_POST['phone'];
              $avatar = $_FILES['avatar']['name'];
              
              //SQL Details
              $sqlServer = 'localhost';
              $sqlUser = 'root';
              $sqlPassword = '';
              $sqlDatabase = '3-5';

              $conn = new mysqli($sqlServer, $sqlUser, $sqlPassword, $sqlDatabase);
              
              $query="INSERT INTO userdata 
                      VALUES ('$username', '$password', '$firstName', 
                              '$lastName', '$birthday', '$email', '$phone', '$avatar');";

              if ($conn->query($query) === true) {
                  $conn->close();
                  header('Location:3-5.php');
              }

              echo "Error";
          } 
      }

      function validate() {
          if (!preg_match ('/^[a-zA-Z0-9\s]+$/', $_POST['username'])) {
              echo 'Invalid Username';
              return false;
          }

          if (!preg_match ('/^[a-zA-Z0-9\-+@.]+$/', $_POST['password'])) {
              echo 'Invalid Password';
              return false;
          }
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['firstName'])) {
              echo 'Invalid First Name';
              return false;
          }
          
          if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['lastName'])) {
            echo 'Invalid Last Name';
            return false;
          }

          if (empty($_POST["birthday"]) == true) {
            echo 'Invalid Date of Birth';
            return false;
          }
          
          if (!preg_match ('/^[a-zA-Z0-9\-+@.]+$/', $_POST['email'])) {
            echo 'Invalid Email Address';
            return false;
          }
          
          define('PHONE_NUMBER_LENGTH', 8);
          if (!strlen($_POST['phone']) == PHONE_NUMBER_LENGTH) {
            echo 'Invalid Phone Number';
            return false;
          }
          
          $tempFile = getimagesize(($_FILES['avatar']['tmp_name']));
          
          if ($tempFile === false){
            echo 'Invalid file selected';
            return false;
          }

          return true;
      }
	?>
  </body>
</html>