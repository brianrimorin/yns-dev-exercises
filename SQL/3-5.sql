-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2022 at 06:10 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3-5`
--

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `username` varchar(20) DEFAULT NULL,
  `user_password` varchar(20) DEFAULT NULL,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `phone` varchar(8) DEFAULT NULL,
  `avatar` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`username`, `user_password`, `firstName`, `lastName`, `birthday`, `email`, `phone`, `avatar`) VALUES
('user1', 'test12', 'test', 'asda', '2012-03-01', 'brianrimorin.yns@gmail.com', '11111111', 'food_fugusashi.png'),
('user2', 'password', 'two', 'test', '2022-02-28', 'brianrimorin.yns@gmail.com', '11112222', 'fish_fugu_haku.png'),
('user3', 'password', 'three', 'test', '2022-02-28', 'brianrimorin.yns@gmail.com', '11112222', 'fish_fugu2.png'),
('user4', 'password', 'four', 'sd', '2022-03-10', 'brianrimorin.yns@gmail.com', '87464123', 'fish_fugu_chouchin.png'),
('user5', 'password', 'five', 'asdfasdf', '2022-03-08', 'brianrimorin.yns@gmail.com', '12351232', 'fish_fugu_chouchin.png'),
('user6', 'password', 'six', 'test', '2022-03-01', 'brianrimorin.yns@gmail.com', '87594265', 'fish_fugu_chouchin.png'),
('user7', 'password', 'seven', 'sd', '2022-03-01', 'brianrimorin.yns@gmail.com', '74859654', 'fish_fugu2.png'),
('user8', 'password', 'eight', 'test', '2022-03-16', 'brianrimorin.yns@gmail.com', '12343222', 'fish_fugu_chouchin.png'),
('user9', 'password', 'nine', 'test', '2022-03-16', 'brianrimorin.yns@gmail.com', '12343222', 'fish_fugu_haku.png'),
('user10', 'password', 'ten', 'test', '2022-03-16', 'brianrimorin.yns@gmail.com', '12343222', 'fish_fugu_chouchin.png'),
('user11', 'password', 'eleven', 'test', '2022-02-28', 'brianrimorin.yns@gmail.com', '12222121', 'fish_fugu_haku.png'),
('user12', 'password', 'twelve', 'sd', '2022-03-01', 'brianrimorin.yns@gmail.com', '55959586', 'fish_fugu2.png'),
('user13', 'password', 'thirteen', 'sd', '2022-03-01', 'brianrimorin.yns@gmail.com', '55959586', 'fish_fugu_chouchin.png'),
('user14', 'password', 'fourteen', 'sd', '2022-03-01', 'brianrimorin.yns@gmail.com', '55959586', 'fish_fugu_haku.png'),
('user15', 'password', 'fifteen', 'test', '2022-03-01', 'brianrimorin.yns@gmail.com', '66666666', 'fish_fugu_chouchin.png'),
('user16', 'password', 'sixteen', 'test', '2022-03-01', 'brianrimorin.yns@gmail.com', '66666666', 'fish_fugu2.png'),
('test', 'test', 'apple', 'apple', '2022-03-01', 'brianrimorin.yns@gmail.com', '00000000', 'food_fugusashi.png'),
('sqluser', '123', 'abc', 'aaaaa', '2020-01-20', 'brianrimorin.yns@gmail.com', '69542365', 'ai_pet_family.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
